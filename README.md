# Estudo pipeline

Projeto para explorar a proposta de pipeline completo
![](../../Pictures/photo_2021-11-06_18-14-08.jpg)

```mermaid
graph LR;
    A[compile];
    A-->B;
    B[checklist];
    B-->C[test component];
    B-->D[test integration];
    B-->E[test metric quality];
    B-->F[test scanner];
    B-->G[test lint];
    C-->H;
    D-->H;
    E-->H;
    F-->H;
    G-->H;
    H{code review};
```

# Arquivos de Configuração

[README](README.md)

[.gitlab-ci](.gitlab-ci.yml)

[dockerfile](Dockerfile)