#!/bin/bash

# Script para verificar se os arquivos de configuração existem 
# Busca os endereço dos arquivos de configuração do final do arquivo 
# readme.md onde deve existir um sessão “# Arquivo de configurações”
# Por fim verifica se os arquivos estão presentes nos respectivos endereços caso não 
# estejam o programa encerra com erro   


while IFS= read -r line
do
    
    if [ "$line" = "# Arquivos de Configuração" ]; then
        break
    fi

    ARQUIVO=$(echo "$line" | grep -o "([A-Za-z]*.*[a-z]*)" | tr -d '()')

   
    if [ ! -e  $ARQUIVO ];
    then
        echo "Esta faltando o arquivo $ARQUIVO"
        exit 1;
    fi

done < <(tac $1)