FROM node:17-alpine3.12

LABEL name="docker-lint"

LABEL Version="dev"

RUN npm install -g dockerfile_lint

COPY . ./home

CMD [ "/bin/sh" ]